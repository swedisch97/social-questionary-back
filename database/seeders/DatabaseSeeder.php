<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Answer;
use App\Models\AnswerOption;
use App\Models\Question;
use App\Models\Questionnaire;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        Questionnaire::factory(3)->has(
            Question::factory(5)
                ->has(
                Answer::factory(1)
            )->has(
                AnswerOption::factory(3)
            ),
        )->create();
    }
}

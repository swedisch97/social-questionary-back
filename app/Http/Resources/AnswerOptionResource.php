<?php

namespace App\Http\Resources;

use App\Models\AnswerOption;
use Illuminate\Http\Resources\Json\JsonResource;

class AnswerOptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
          'value' => AnswerOption::get('value'),
          'question_id' => AnswerOption::get('question_id')
        ];
    }
}

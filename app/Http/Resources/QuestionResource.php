<?php

namespace App\Http\Resources;

use App\Models\Question;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => Question::get('id'),
            'name' => Question::get('name'),
            'questionnaire_id'=>Question::get('questionnaire_id'),
            'description' => Question::get('description'),
            'has_custom_answer' => Question::get('has_custom_answer')
        ];
    }
}

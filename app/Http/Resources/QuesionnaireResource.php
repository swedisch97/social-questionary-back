<?php

namespace App\Http\Resources;

use App\Models\Questionnaire;
use Illuminate\Http\Resources\Json\JsonResource;

class QuesionnaireResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>Questionnaire::get('id'),
            'name'=>Questionnaire::get('name'),
            'questionnaire_id' => Questionnaire::get('questionnaire_id')
        ];
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateQuestionnaireRequest;
use App\Http\Resources\QuesionnaireResource;
use App\Models\Questionnaire;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    public function index(Request $request)
    {
        return Questionnaire::
        orderByDesc('sort')->paginate($request->get('paginate'));
    }

    public function show($id)
    {
        return Questionnaire::query()
            ->with('sections', function ($query) {
                $query->orderBy('sort', 'asc');
            })
            ->with([
                'sections.questions.answers',
                'sections.questions.questions.answers',
                'sections.questions.questions.answerOptions',
                'sections.questions.answerOptions'
            ])->findOrFail($id);
    }

    public function stats() {
        $stats = Questionnaire::query()->whereNull('questionnaire_id')->with([
            'sections.questions.answers',
            'sections.questions.questions.answers',
            'sections.questions.questions.answerOptions',
            'sections.questions.questions.answerOptions',
            'sections.questions.answerOptions'
        ])->get()->each(function (Questionnaire $questionnaire) {
            $questionnaire['questions_count'] = $questionnaire->sections->sum(function (Questionnaire $section) {
                return $section->questions->count();
            });

            $countAnswers = 0;

            $questionnaire->sections->each(function ($section) use (&$countAnswers) {
                $countAnswers +=  $section->questions->sum(fn ($question) => $question->answers->count());
            });

            $questionnaire['answers_count'] = $countAnswers;
        });

        return view('stats')->with([
            'stats' => $stats
        ]);
    }

    public function store(CreateQuestionnaireRequest $request){

        $validated = $request->validated();
        $questionnaire = Questionnaire::create($validated);
        return QuesionnaireResource::make($questionnaire);
    }

    public function delete($id){
        Questionnaire::query()->where('id','=',$id)->delete();
        return response('Анкета удалена',200);
    }
}

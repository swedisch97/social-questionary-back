<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateQuestionRequest;
use App\Http\Resources\QuestionResource;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{

    public function store(CreateQuestionRequest $request){

        $validated = $request->validated();
        $question = Question::create($validated);
        return QuestionResource::make($question);
    }

    public function delete($id){
        Question::query()->where('id','=',$id)->delete();
        return response('Вопрос удален',200);
    }

    public function index()
    {
        return Question::all();
    }

    public function show($id){
        return Question::findOrFail($id)->toArray();
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAnswerOptionRequest;
use App\Http\Resources\AnswerOptionResource;
use App\Models\AnswerOption;
use Illuminate\Http\Request;

class AnswerOptionController extends Controller
{

    public function store(CreateAnswerOptionRequest $request){

        $validated = $request->validated();
        $question = AnswerOption::create($validated);
        return AnswerOptionResource::make($question);
    }

    public function delete($id){
        AnswerOption::query()->where('id','=',$id)->delete();
        return response('Вариант ответа удален',200);
    }

    public function index(Request $request)
    {
        return AnswerOption::all();
    }

    public function show($id){
        return AnswerOption::findOrFail($id)->toArray();
    }
}

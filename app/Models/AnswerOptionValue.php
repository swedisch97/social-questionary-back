<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AnswerOptionValue
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOptionValue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOptionValue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOptionValue query()
 * @mixin \Eloquent
 */
class AnswerOptionValue extends Model
{
    use HasFactory;

   protected $guarded = [
       'id'
   ];
}

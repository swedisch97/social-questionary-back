<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AnswerOption
 *
 * @property int $id
 * @property string $value
 * @property int $question_id
 * @property int|null $answer_options_value_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\AnswerOptionValue|null $answerOptionValue
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Answer[] $answers
 * @property-read int|null $answers_count
 * @property-read \App\Models\Question $question
 * @method static \Database\Factories\AnswerOptionFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption whereAnswerOptionsValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOption whereValue($value)
 * @mixin \Eloquent
 */
class AnswerOption extends Model
{
    use HasFactory;

   protected $guarded = [
       'id'
   ];

   public function question() {
       return $this->belongsTo(Question::class);
   }

   public function answerOptionValue() {
       return $this->belongsTo(AnswerOptionValue::class);
   }

   public function answers()
   {
       return $this->hasMany(Answer::class);
   }

   public function countAnswers()
   {
       return $this->answers->count();
   }
}

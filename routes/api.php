<?php

use App\Http\Controllers\API\AnswerOptionController;
use App\Http\Controllers\API\QuestionController;
use App\Http\Controllers\API\QuestionnaireController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('questionnaires', QuestionnaireController::class);

Route::apiResource('questions', QuestionController::class);

Route::apiResource('options', AnswerOptionController::class);










<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    @foreach($stats as $stat)
        <div>
            <h1>Название опроса: {{$stat->name}}</h1>
            <br>
            <h2>Кол-во вопросов: {{$stat->questions_count}}</h2>
            <h2>Кол-во ответов: {{$stat->answers_count}}</h2>
            <br>

            @foreach($stat->questions as $question)
                <h3>Вопрос:{{$question->name}}</h3>
                <p>Варинаты ответа: </p>
                @foreach($question->answerOptions as $option)
                    <p>
                        {{$option->value}} : <h3>проголосовало - {{$option->answers->count()}}</h3>
                    </p>
                @endforeach
                <br>

            @endforeach
        </div>
    @endforeach
</body>
</html>
